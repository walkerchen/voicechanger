package com.lehe.voicechanger;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.UUID;

import android.os.Bundle;
import android.provider.Settings.Secure;
import android.app.Activity;
import android.content.Context;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class VCActivity extends Activity {

	final int MAX_SHORT = 32767;
	final int MIN_SHORT = -32767;
	
	static
    {
        System.loadLibrary("soundtouch");
    }
	
	private final String TAG = "VCActivity";
	
	TextView info;
	
	Button btnFemale;
	Button btnMale;
	Button btnRobot;
	Button btnAirport;
	Button btnBar;
	Button btnMeeting;
	Button btnRecord;
	Button btnPlay;
	
	SoundTouchRecorderNative soundTouchRec;
	private String lastRecordFile;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_vc);
		
		initUI();
		
		initSoundTouch();
		
		readDeviceID();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.vc, menu);
		return true;
	}
	
	void readDeviceID()
	{
		final TelephonyManager tm = (TelephonyManager) getBaseContext().getSystemService(Context.TELEPHONY_SERVICE);

	    final String tmDevice, tmSerial, androidId;
	    tmDevice = "" + tm.getDeviceId();
	    tmSerial = "" + tm.getSimSerialNumber();
	    androidId = "" + android.provider.Settings.Secure.getString(getContentResolver(), android.provider.Settings.Secure.ANDROID_ID);

	    UUID deviceUuid = new UUID(androidId.hashCode(), ((long)tmDevice.hashCode() << 32) | tmSerial.hashCode());
	    String deviceId = deviceUuid.toString();
	    
	    info = (TextView) findViewById(R.id.Info);
	    Log.d(this.getPackageName(), tmDevice);
	    info.setText(tmDevice);
	}
	
	void initUI()
	{
		btnFemale = (Button) findViewById(R.id.btnFemale);
		btnFemale.setOnClickListener(femaleChangeListener);
		
		btnMale = (Button) findViewById(R.id.btnMale);
		btnMale.setOnClickListener(maleChangeListener);
		
		btnRobot = (Button) findViewById(R.id.btnRobot);
		btnRobot.setOnClickListener(normListener);

		btnAirport = (Button) findViewById(R.id.btnAirport);
		btnBar = (Button) findViewById(R.id.btnBar);
		
		btnMeeting = (Button) findViewById(R.id.btnMeeting);
		btnMeeting.setOnClickListener(meetingChangeListener);

		btnRecord = (Button) findViewById(R.id.btnRecord);
		btnRecord.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN ) {
                	changeSound(0f, 10f);
        			soundTouchRec.startRecord();
        			Log.d(TAG, "startRecord");
                    return true;
                }
                else if (event.getAction() == MotionEvent.ACTION_UP || event.getAction() == MotionEvent.ACTION_CANCEL )
                {
        			lastRecordFile = soundTouchRec.stopRecorder();
        			Log.d(TAG, "lastRecordFile=" + lastRecordFile);
                }
                return false;
            }
        });
		
		btnPlay =  (Button) findViewById(R.id.btnPlay);
		btnPlay.setOnClickListener(voicePlayListener);
	}
	
	private void changeSound(float pitch, float tempo)
	{
		NativeSoundTouch.getSoundTouch().setPitchSemiTones(pitch);
		NativeSoundTouch.getSoundTouch().setTempoChange(tempo);
		soundTouchRec.startConvert();
	}

	private OnClickListener maleChangeListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			changeSound(-6.5f, 10f);
		}
	};

	private OnClickListener femaleChangeListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			changeSound(8f, -15.0f);
		}
	};

	private OnClickListener meetingChangeListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			changeSound(1, 10);
		}
	};

	private OnClickListener voicePlayListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			soundTouchRec.startPlay();
		}
	};
	
	private OnClickListener normListener = new OnClickListener() {
		@Override
		public void onClick(View v) {
			try
			{
				boolean result = normlize();
				Toast.makeText(getBaseContext(),"Normalized completed " + result, 
		                Toast.LENGTH_SHORT).show();
			}catch(Exception e)
			{
				e.printStackTrace();
			}
		}
	};

	boolean normlize()
		throws Exception
	{
		String destFile = "norm.pcm";
		String srcFile = "last_record.pcm";
		
		FileInputStream pcmStream = this.openFileInput(srcFile);

	    byte buffer[] = new byte[pcmStream.available()];
    	pcmStream.read(buffer);
		
		/* find the peak */
    	float max = 0;
    	float min = 0;
		for (int i=0; i<buffer.length; i+=2) {
			if(i+1>=buffer.length)	break;
			
			//int unit = (int)(((buffer[i]&0xff)<<8) + (buffer[i+1]&0xff));

			byte[] tmpBuf = new byte[2];
			tmpBuf[0] = buffer[i];
			tmpBuf[1] = buffer[i+1];
			ByteBuffer tmp = ByteBuffer.wrap(tmpBuf);
			tmp.order(ByteOrder.LITTLE_ENDIAN);  // if you want little-endian
			short unit = tmp.getShort();
			if(max<unit) max=unit;
			if(min>unit) min=unit;
		}
		
		Log.d(TAG, "Max=" + max +", min=" + min);
		
		if(max!=0 || min!=0)
		//if(false)
		{
			float maxMul = Math.abs((float)MAX_SHORT/max);
			float minMul = Math.abs((float)MIN_SHORT/max);
			float mul = (maxMul<minMul) ? maxMul : minMul;
			Log.d(TAG, "mul=" + mul);
			
			/* apply normalization */
			for (int i=0; i<buffer.length; i+=2) {
				if(i+1>=buffer.length)	break;
				
				//int unit = (int)(((buffer[i]&0xff)<<8) + (buffer[i+1]&0xff));

				byte[] tmpBuf = new byte[2];
				tmpBuf[0] = buffer[i];
				tmpBuf[1] = buffer[i+1];
				ByteBuffer tmp = ByteBuffer.wrap(tmpBuf);
				tmp.order(ByteOrder.LITTLE_ENDIAN);  // if you want little-endian
				short unit = tmp.getShort();

				int amp = (int)(unit * mul * 1.5);
				if(unit>MAX_SHORT || unit<MIN_SHORT)
					Log.d(TAG, "mul="+mul + ", unit=" + unit + ", amp=" + amp);

				if(amp>MAX_SHORT) unit=MAX_SHORT;
				else if(amp<MIN_SHORT) unit=MIN_SHORT;
				else unit = (short)amp;
				
				
				ByteBuffer dbuf = ByteBuffer.allocate(2);
				dbuf.order(ByteOrder.LITTLE_ENDIAN);
				dbuf.putShort(unit);
				byte[] normBytes = dbuf.array();

				/*
				byte[] normBytes = new byte[2];
				normBytes[0] = (byte)(unit & 0xff);
				normBytes[1] = (byte)((unit & 0xff00)>>8);
				*/
				if(normBytes.length==2)
				{
					buffer[i] = normBytes[0];
					buffer[i+1] = normBytes[1];
				}
			}
		}

		FileOutputStream normStream = this.openFileOutput(destFile, Context.MODE_PRIVATE);
		normStream.write(buffer);
		
		return true;
	}
	
	void initSoundTouch()
	{	
		soundTouchRec = new SoundTouchRecorderNative(this);
	}
	
	@Override
	protected void onPause() {
		super.onPause();
		
		soundTouchRec.stopRecorder();
		soundTouchRec.stopPlay();
	}
}
